/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class NodeAdvanced<T extends Comparable> implements Comparable<NodeAdvanced> {

    private T data;
    private NodeAdvanced<T> leftNode, rightNode;

    NodeAdvanced(T data) {
        this(data, null, null);
    }

    NodeAdvanced(T data, NodeAdvanced<T> leftNode, NodeAdvanced<T> rightNode) {
        this.data = data;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    @Override
    public int compareTo(NodeAdvanced nodeAdvanced) {
        return getData().compareTo(nodeAdvanced.getData());
    }

    public T getData() {
        return data;
    }

    public NodeAdvanced<T> getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(NodeAdvanced<T> leftNode) {
        this.leftNode = leftNode;
    }

    public NodeAdvanced<T> getRightNode() {
        return rightNode;
    }

    public void setRightNode(NodeAdvanced<T> rightNode) {
        this.rightNode = rightNode;
    }
}
