/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class NodeBasic implements Comparable<NodeBasic> {

    private String data;
    private NodeBasic leftNode, rightNode;

    NodeBasic(String data) {
        this(data, null, null);
    }

    NodeBasic(String data, NodeBasic leftNode, NodeBasic rightNode) {
        this.data = data;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    @Override
    public int compareTo(NodeBasic nodeBasic) {
        return getData().compareTo(nodeBasic.getData());
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public NodeBasic getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(NodeBasic leftNode) {
        this.leftNode = leftNode;
    }

    public NodeBasic getRightNode() {
        return rightNode;
    }

    public void setRightNode(NodeBasic rightNode) {
        this.rightNode = rightNode;
    }

}
