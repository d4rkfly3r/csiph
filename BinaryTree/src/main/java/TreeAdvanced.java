/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class TreeAdvanced<T extends Comparable> {

    private NodeAdvanced<T> rootNode;

    TreeAdvanced() {
        this(null);
    }

    TreeAdvanced(NodeAdvanced<T> rootNode) {
        this.rootNode = rootNode;
    }

    public NodeAdvanced<T> getRootNode() {
        return rootNode;
    }

    public int getSize() {
        return rootNode == null ? 0 : getSize(rootNode);
    }

    public int getSize(NodeAdvanced<T> nodeAdvanced) {
        return nodeAdvanced == null ? 0 : (1 + getSize(nodeAdvanced.getLeftNode()) + getSize(nodeAdvanced.getRightNode()));
    }

    public void add(T data) {
        NodeAdvanced<T> tempNode = new NodeAdvanced<T>(data);
        if (this.rootNode == null) {
            this.rootNode = tempNode;
        } else {
            NodeAdvanced<T> parentNode, currentNode = this.rootNode;
            while (true) {
                parentNode = currentNode;
                if (data.compareTo(currentNode.getData()) < 0) {
                    // Set currentNode to equal the currentNode's leftNode, and then check if it equals null
                    if ((currentNode = currentNode.getLeftNode()) == null) {
                        parentNode.setLeftNode(tempNode);
                        break;
                    }
                } else {
                    // Set currentNode to equal the currentNode's rightNode, and then check if it equals null
                    if ((currentNode = currentNode.getRightNode()) == null) {
                        parentNode.setRightNode(tempNode);
                        break;
                    }
                }
            }
        }
    }

    public String inOrder() {
        return inOrder(rootNode);
    }

    public String inOrder(NodeAdvanced<T> node) {
        // Ternary Operators.... GOOGLE IT
        return node == null ? "" : inOrder(node.getLeftNode()) + node.getData() + inOrder(node.getRightNode());
    }

    public String preOrder() {
        return preOrder(rootNode);
    }

    public String preOrder(NodeAdvanced<T> node) {
        // Ternary Operators.... GOOGLE IT
        return node == null ? "" : node.getData() + preOrder(node.getLeftNode()) + preOrder(node.getRightNode());
    }

    public String postOrder() {
        return postOrder(rootNode);
    }

    public String postOrder(NodeAdvanced<T> node) {
        // Ternary Operators.... GOOGLE IT
        return node == null ? "" : postOrder(node.getLeftNode()) + postOrder(node.getRightNode()) + node.getData();
    }


}
