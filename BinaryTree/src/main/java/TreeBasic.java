/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class TreeBasic {

    private NodeBasic rootNode;

    TreeBasic() {
        this(null);
    }

    TreeBasic(NodeBasic rootNode) {
        this.rootNode = rootNode;
    }

    public NodeBasic getRootNode() {
        return rootNode;
    }

    public int getSize() {
        return rootNode == null ? 0 : getSize(rootNode);
    }

    public int getSize(NodeBasic node) {
        return node == null ? 0 : (1 + getSize(node.getLeftNode()) + getSize(node.getRightNode()));
    }

    public void add(String data) {
        NodeBasic tempNode = new NodeBasic(data);
        if (this.rootNode == null) {
            this.rootNode = tempNode;
        } else {
            NodeBasic parentNode, currentNode = this.rootNode;
            while (true) {
                parentNode = currentNode;
                if (data.compareTo(currentNode.getData()) < 0) {
                    // Set currentNode to equal the currentNode's leftNode, and then check if it equals null
                    if ((currentNode = currentNode.getLeftNode()) == null) {
                        parentNode.setLeftNode(tempNode);
                        break;
                    }
                } else {
                    // Set currentNode to equal the currentNode's rightNode, and then check if it equals null
                    if ((currentNode = currentNode.getRightNode()) == null) {
                        parentNode.setRightNode(tempNode);
                        break;
                    }
                }
            }
        }
    }

    public String inOrder() {
        return inOrder(rootNode);
    }

    public String inOrder(NodeBasic node) {
        // Ternary Operators.... GOOGLE IT
        return node == null ? "" : inOrder(node.getLeftNode()) + node.getData() + inOrder(node.getRightNode());
    }

    public String preOrder() {
        return preOrder(rootNode);
    }

    public String preOrder(NodeBasic node) {
        // Ternary Operators.... GOOGLE IT
        return node == null ? "" : node.getData() + preOrder(node.getLeftNode()) + preOrder(node.getRightNode());
    }

    public String postOrder() {
        return postOrder(rootNode);
    }

    public String postOrder(NodeBasic node) {
        // Ternary Operators.... GOOGLE IT
        return node == null ? "" : postOrder(node.getLeftNode()) + postOrder(node.getRightNode()) + node.getData();
    }
}
