/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class NodeAdvancedTests extends TestThread {

    @Override
    public void run() {
        super.run();

        TreeAdvanced<Integer> treeBasic = new TreeAdvanced<>();

//        treeBasic.add("J");
//        treeBasic.add("O");
//        treeBasic.add("S");
//        treeBasic.add("H");
//        treeBasic.add("U");
//        treeBasic.add("A");

        treeBasic.add(1);
        treeBasic.add(6);
        treeBasic.add(2);
        treeBasic.add(3);
        treeBasic.add(5);
        treeBasic.add(4);

        System.out.println(treeBasic.inOrder());
        System.out.println(treeBasic.preOrder());
        System.out.println(treeBasic.postOrder());
        System.out.println(treeBasic.getSize());
    }
}
