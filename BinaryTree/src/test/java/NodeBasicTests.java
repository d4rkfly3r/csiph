/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class NodeBasicTests extends TestThread {

    @Override
    public void run() {
        super.run();

        TreeBasic treeBasic = new TreeBasic();

        treeBasic.add("J");
        treeBasic.add("O");
        treeBasic.add("S");
        treeBasic.add("H");
        treeBasic.add("U");
        treeBasic.add("A");
        treeBasic.add(" ");
        treeBasic.add("F");
        treeBasic.add("R");
        treeBasic.add("E");
        treeBasic.add("E");
        treeBasic.add("D");
        treeBasic.add("M");
        treeBasic.add("A");
        treeBasic.add("N");

        System.out.println(treeBasic.inOrder());
        System.out.println(treeBasic.preOrder());
        System.out.println(treeBasic.postOrder());
        System.out.println(treeBasic.getSize());
    }
}
