/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class ConstantNode implements ExpressionNodeInterface {


    private Number data;

    public ConstantNode() {
        this(null);
    }

    public ConstantNode(Number data) {
        this.data = data;
    }

    public void setData(Number data) {
        this.data = data;
    }

    @Override
    public Number getData() {
        return data;
    }
}
