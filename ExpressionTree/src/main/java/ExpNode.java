/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class ExpNode extends OperationNode implements ExpressionNodeInterface {

    ExpNode(ExpressionNodeInterface... interfaces) {
        super(interfaces);
    }

    @Override
    public Number getData() {
        Double i = null;
        for (ExpressionNodeInterface expressionNodeInterface : getChildNodes()) {
            if (i == null) i = expressionNodeInterface.getData().doubleValue();
            else
                i = Math.pow(i, expressionNodeInterface.getData().doubleValue());
        }
        return i;
    }
}
