/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public interface ExpressionNodeInterface {
    Number getData();
}
