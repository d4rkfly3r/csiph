/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class LogNode extends OperationNode implements ExpressionNodeInterface {

    LogNode(ExpressionNodeInterface... interfaces) {
        super(interfaces);
    }

    @Override
    public Number getData() {
        return Math.log(getChildNodes().get(0) == null ? 0 : getChildNodes().get(0).getData().doubleValue());
    }
}
