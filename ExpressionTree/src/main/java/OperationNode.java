import java.util.Arrays;
import java.util.List;

/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class OperationNode {
    private List<ExpressionNodeInterface> nodes;

    OperationNode() {
        this(null, null);
    }

    OperationNode(ExpressionNodeInterface... nodes) {
        this.nodes = Arrays.asList(nodes);
    }

    public List<ExpressionNodeInterface> getChildNodes() {
        return nodes;
    }

    public ExpressionNodeInterface getNode(int index) {
        return nodes.get(index);
    }

    public void setNode(int index, ExpressionNodeInterface node) {
        this.nodes.add(index, node);
    }

    public void addNode(int index, ExpressionNodeInterface node) {
        this.nodes.add(node);
    }
}
