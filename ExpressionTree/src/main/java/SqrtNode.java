/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class SqrtNode extends OperationNode implements ExpressionNodeInterface {

    SqrtNode(ExpressionNodeInterface... interfaces) {
        super(interfaces);
    }

    @Override
    public Number getData() {
        return Math.sqrt(getChildNodes().get(0) == null ? 0 : getChildNodes().get(0).getData().doubleValue());
    }
}
