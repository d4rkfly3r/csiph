/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class SubtractionNode extends OperationNode implements ExpressionNodeInterface {

    SubtractionNode(ExpressionNodeInterface... interfaces) {
        super(interfaces);
    }

    @Override
    public Number getData() {
        Double i = null;
        for (ExpressionNodeInterface expressionNodeInterface : getChildNodes()) {
            if (i == null) i = expressionNodeInterface.getData().doubleValue();
            else
                i -= expressionNodeInterface.getData().doubleValue();
        }
        return i;
    }
}
