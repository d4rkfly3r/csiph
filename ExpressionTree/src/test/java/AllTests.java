/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class AllTests {

    public static void main(String[] args) {

        PostfixConverter postfixConverter = new PostfixConverter("2 RAD ((5*4)+6-1)");
        Parser parser = new Parser();
        parser.execute(postfixConverter.convertInfixExpression());

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("Answer: " + parser.nodeInterfaceStack.peek().getData());
        parser.printThorough(parser.nodeInterfaceStack.pop(), "");

    }
}
