import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class Lexer {

    private static Lexer ourInstance = new Lexer();

    public static Lexer getInstance() {
        return ourInstance;
    }

    private Lexer() {
    }

    public enum TokenType {
        NUMBER("-?[0-9]+"), BINARYOP("[*|/|+|-|%]"), WHITESPACE("[ \t\f\r\n]+"), OPENPAR("\\("), CLOSEPAR("\\)"), OTHER("\\S\\w+");

        public final String pattern;

        TokenType(String pattern) {
            this.pattern = pattern;
        }
    }

    public static class Token {
        public final TokenType tokenType;
        public final String data;

        public Token(TokenType tokenType, String data) {
            this.tokenType = tokenType;
            this.data = data;
        }

        @Override
        public String toString() {
            return String.format("(%s %s)", tokenType.name(), data);
        }
    }

    public static ArrayList<Token> lex(String inputString) {
        ArrayList<Token> tokens = new ArrayList<>();

        StringBuilder tokenPatternsBuffer = new StringBuilder();
        for (TokenType tokenType : TokenType.values()) {
            tokenPatternsBuffer.append(String.format("|(?<%s>%s)", tokenType.name(), tokenType.pattern));
        }

        Pattern tokenPatterns = Pattern.compile(tokenPatternsBuffer.substring(1));

        Matcher matcher = tokenPatterns.matcher(inputString);
        while (matcher.find()) {
            if (matcher.group(TokenType.NUMBER.name()) != null) {
                tokens.add(new Token(TokenType.NUMBER, matcher.group(TokenType.NUMBER.name())));
            } else if (matcher.group(TokenType.BINARYOP.name()) != null) {
                tokens.add(new Token(TokenType.BINARYOP, matcher.group(TokenType.BINARYOP.name())));
            } else if (matcher.group(TokenType.WHITESPACE.name()) != null) {
            } else if (matcher.group(TokenType.OPENPAR.name()) != null) {
                tokens.add(new Token(TokenType.OPENPAR, matcher.group(TokenType.OPENPAR.name())));
            } else if (matcher.group(TokenType.CLOSEPAR.name()) != null) {
                tokens.add(new Token(TokenType.CLOSEPAR, matcher.group(TokenType.CLOSEPAR.name())));
            } else {
                tokens.add(new Token(TokenType.OTHER, matcher.group(TokenType.OTHER.name())));
            }
        }

        return tokens;
    }

    public static void main(String[] args) {
        String input = "((1 + 2) - 3)";

        ArrayList<Token> tokenArrayList = lex(input);
    }
}
