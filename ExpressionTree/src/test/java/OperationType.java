/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public enum OperationType {
    ADD, SUBTRACT, MULTIPLY, DIVIDE, MODULO
}
