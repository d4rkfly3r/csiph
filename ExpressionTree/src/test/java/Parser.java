import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

/**
 * Created by Joshua Freedman on 10/25/2015.
 */
public class Parser {
    Stack<ExpressionNodeInterface> nodeInterfaceStack = new Stack<>();

    HashMap<String, LanguagePart> dictionary = new HashMap<>();
    Iterator<String> lexer;

    Parser() {
        {
            addWord("PRINT", mainClass -> {
                if (nodeInterfaceStack.size() < 1) {
                    System.err.println("Not enough items on stack!");
                } else {
                    System.out.println(nodeInterfaceStack.pop().getData());
                }
            });
            addWord("PSTACK", mainClass -> {
                nodeInterfaceStack.forEach(dataValue -> {
                    System.out.print(dataValue.getData() + "; ");
                });
            });
        }
        {
            addWord("+", mainClass -> {
                if (nodeInterfaceStack.size() < 2) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                ExpressionNodeInterface eni2 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new AdditionNode(eni2, eni1));
            });
            addWord("-", mainClass -> {
                if (nodeInterfaceStack.size() < 2) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                ExpressionNodeInterface eni2 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new SubtractionNode(eni2, eni1));
            });
            addWord("*", mainClass -> {
                if (nodeInterfaceStack.size() < 2) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                ExpressionNodeInterface eni2 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new MultiplicationNode(eni2, eni1));
            });
            addWord("/", mainClass -> {
                if (nodeInterfaceStack.size() < 2) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                ExpressionNodeInterface eni2 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new DivisionNode(eni2, eni1));
            });
            addWord("RAD", mainClass -> {
                if (nodeInterfaceStack.size() < 2) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                ExpressionNodeInterface eni2 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new ExpNode(eni1, new DivisionNode(new ConstantNode(1), eni2)));
            });
            addWord("log", mainClass -> {
                if (nodeInterfaceStack.size() < 1) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new LogNode(eni1));
            });
            addWord("^", mainClass -> {
                if (nodeInterfaceStack.size() < 2) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                ExpressionNodeInterface eni2 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new ExpNode(eni2, eni1));
            });
            addWord("%", mainClass -> {
                if (nodeInterfaceStack.size() < 2) System.err.println("Not enough items on stack!");

                ExpressionNodeInterface eni1 = mainClass.nodeInterfaceStack.pop();
                ExpressionNodeInterface eni2 = mainClass.nodeInterfaceStack.pop();
                nodeInterfaceStack.push(new ModuloNode(eni2, eni1));
            });
        }
        {
            addWord("DUPE", mainClass -> {
                if (nodeInterfaceStack.size() < 1) System.err.println("Not enough items on stack!");
                ExpressionNodeInterface dataValue = nodeInterfaceStack.pop();
                nodeInterfaceStack.push(dataValue);
                nodeInterfaceStack.push(dataValue);
            });
            addWord("DROP", mainClass -> {
                if (nodeInterfaceStack.size() < 1) System.err.println("Not enough items on stack!");
                nodeInterfaceStack.pop();
            });
        }
        {
            addWord("VAR", mainClass -> {
                String nCall = lexer.next();
                if (nCall == null) System.err.println("Unexpected EOI");
                addWord(nCall.toUpperCase(), mainClass1 -> nodeInterfaceStack.push(new ConstantNode(0)));
            });
            addWord("FETCH", mainClass -> {
                if (nodeInterfaceStack.size() < 1) System.err.println("Not enough items on stack!");
                nodeInterfaceStack.pop();
            });
        }
        {
            addWord("/*", mainClass -> {
                String nWord = "";
                do {
                    nWord = mainClass.lexer.next();
                    if (nWord == null) System.err.println("Unexpected EOI!");
                } while (!nWord.endsWith("*/"));
            });
        }
    }

    public void printThorough(ExpressionNodeInterface peek, String idlevel) {
        if (peek instanceof OperationNode) {
            OperationNode on = (OperationNode) peek;
            for (ExpressionNodeInterface eni : on.getChildNodes()) {
                printThorough(eni, idlevel + "\t");
            }
        } else if (peek instanceof ConstantNode) {
            ConstantNode cn = (ConstantNode) peek;
            System.out.println(idlevel + cn.getData());
        }
    }

    public Iterator<String> lexrenate(String text) {
        String[] words = text.replace("\"", "\" ").split("\\s+");
        Iterator<String> strings = Arrays.asList(words).iterator();
        return strings;
    }


    public void addWord(String call, LanguagePart languagePart) {
        dictionary.put(call, languagePart);
    }

    public void execute(String text) {
        lexer = lexrenate(text);
        String word;
        float num_val;

        while (lexer.hasNext()) {
            word = lexer.next().toUpperCase();
            num_val = isFloat(word) ? Float.parseFloat(word) : Float.NaN;
            if (dictionary.containsKey(word)) {
                dictionary.get(word).func(this);
            } else if (!Float.isNaN(num_val)) {
                this.nodeInterfaceStack.push(new ConstantNode(num_val));
            } else {
                System.err.println("Unknown Call");
            }
        }
    }

    private boolean isFloat(String word) {
        try {
            Float.parseFloat(word);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public interface LanguagePart {
        void func(Parser parser);
    }

    public static void main(String[] args) {
        Parser parser = new Parser();

        String t = "25 2 RAD";


        parser.execute(t);//new PostfixConverter(t).convertInfixExpression());

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println(parser.nodeInterfaceStack.peek().getData());
        parser.printThorough(parser.nodeInterfaceStack.pop(), "");

    }
}
