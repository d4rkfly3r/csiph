import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Joshua Freedman on 10/26/2015.
 */
public class PostfixConverter {
    private String infixExpression;

    public PostfixConverter(String infixExpression) {
        this.infixExpression = infixExpression;

    }

    public String convertInfixExpression() {
        Pattern pattern = Pattern.compile("(([0-9]*\\.[0-9]+|[0-9]+))|(\\+)|(\\-)|(\\*)|(\\/)|(\\()|(\\))|(\\%)|(\\s+)|(\\^)|(RAD)");
        Matcher matcher = pattern.matcher(infixExpression);

        Stack<String> operatorStack = new Stack<>();
        String converted = "";
        while (matcher.find()) {
            String token = matcher.group();
            if (token.trim().equalsIgnoreCase("")) continue;
            if (token.equals("(")) {
                operatorStack.push(token);

            } else if (token.equals(")")) {
                while (!operatorStack.peek().equals("(")) {
                    converted = converted + " " + operatorStack.pop();

                }
                if (operatorStack.peek().equals("(")) {
                    operatorStack.pop();

                }

            } else if (isOperator(token)) {
                if (operatorStack.isEmpty()) {
                    operatorStack.push(token);
                } else {
                    if (ICP(token) < ISP(operatorStack.peek())) {
                        converted = converted + " " + operatorStack.pop();
                        operatorStack.push(token);
                    } else {
                        operatorStack.push(token);

                    }

                }

            } else {
                converted = converted + " " + token;

            }

        }
        while (!operatorStack.isEmpty()) {
            converted = converted + " " + operatorStack.pop();

        }
        return converted;

    }

    public int ISP(String token) {
        int precedence = 0;
        if (token.equals("+") || token.equals("-")) {
            precedence = 2;

        } else if (token.equals("*") || token.equals("/")) {
            precedence = 4;
        } else if (token.equals("^")) {
            precedence = 5;

        } else if (token.equals("RAD")) {
            precedence = 3;

        } else if (token.equals("(")) {
            precedence = 0;

        }
        return precedence;

    }

    public int ICP(String token) {
        int precedence = 0;
        if (token.equals("+") || token.equals("-")) {
            precedence = 1;

        } else if (token.equals("*") || token.equals("/")) {
            precedence = 3;

        } else if (token.equals("^")) {
            precedence = 6;

        }else if (token.equals("RAD")) {
            precedence = 2;

        }
        return precedence;

    }

    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/") || token.equals("^") || token.equals("%") || token.equals("RAD"));
    }

    public static void main(String[] args) {
        System.out.println("Input the infix expression: ");
        Scanner input = new Scanner(System.in);
        String infixExpression = input.nextLine();
        PostfixConverter converter = new PostfixConverter(infixExpression);
        System.out.println("The converted expression is " + converter.convertInfixExpression());
    }
}
