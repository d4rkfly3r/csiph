package us.jfreedman.src;

import net.d4.src.*;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 9:10 PM.
 */
@Plugin(name = "MyPlugin3", uniqueID = "uuidindeed3")
public class MainPlugin2 implements Listener {

    @EventHandler
    public void launch(AppInitEvent event) {
        System.out.println("TEST: " + event.getName());
//        PluginManager.register(this);
    }

    @EventHandler
    public void callEvent(LaunchEvent event) {
        System.out.println(event.getName());
    }

}
