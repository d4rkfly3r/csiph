package net.d4.src;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 9:54 PM.
 */
public class AppInitEvent implements Event
{
    @Override
    public String getName() {
        return "This is an init event!";
    }
}
