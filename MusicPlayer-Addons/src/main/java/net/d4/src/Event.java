package net.d4.src;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 9:24 PM.
 */
public interface Event {

    String getName();
}
