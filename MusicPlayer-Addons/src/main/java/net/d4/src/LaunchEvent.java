package net.d4.src;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 9:47 PM.
 */
public class LaunchEvent implements Event {

    String className;

    LaunchEvent(String className) {
        this.className = className;
    }


    @Override
    public String getName() {
        return "Testing this: " + className;
    }
}
