package net.d4.src;

import javafx.application.Platform;
import net.d4.src.detector.ADetect;
import net.d4.src.window.SplashScreenJavaFXApp;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by: Joshua Freedman on Jul 12, 2015 @ 7:56 PM.
 */
public class RelaunchMain {

    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        System.out.println("CP: " + System.getProperty("java.class.path"));

        ArrayList<Class<?>> classArrayList = new ArrayList<>();

        final ADetect.TypeReporter reporter = new ADetect.TypeReporter() {
            @Override
            public void reportTypeAnnotation(Class<? extends Annotation> annotation, String className) {
                System.out.println("FOUND !");
                try {
                    SplashScreenJavaFXApp.setLoadingText(className);
                    classArrayList.add(Class.forName(className));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public Class<? extends Annotation>[] annotations() {
                return new Class[]{Plugin.class};
            }
        };

        System.out.println("STARTING");
        final ADetect aDetect = new ADetect(reporter);

        new Thread(() -> SplashScreenJavaFXApp.main(splashScreenJavaFXApp -> {
            try {
                aDetect.detect();
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (Class<?> clz : classArrayList) {
                splashScreenJavaFXApp.getController().LoadingText.setText("Registering class: " + clz.getName());
                System.out.println("CLS NAME: " + clz.getName());
                System.out.println("CLS ANNO: " + Arrays.toString(clz.getAnnotations()));
                System.out.println("CLS METHS: " + Arrays.toString(clz.getMethods()));
                PluginManager.registerBase(clz);
            }
            Event event = new AppInitEvent();
            PluginManager.callEvent(event);
            PluginManager.callEvent(new LaunchEvent("This is a test launch value"));
            splashScreenJavaFXApp.getController().LoadingText.setText("Initializing Plugins!");

            System.out.println("COMPLETE: " + classArrayList.size());
        })).start();
    }
}
