package net.d4.src.window;/**
 * Created by Joshua Freedman on 10/26/2015.
 */

import javafx.application.Application;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class SplashScreenJavaFXApp extends Application {
    private Stage mainStage;
    private SplashScreenController controller;
    private static SplashScreenJavaFXApp instance;
    private static Consumer<SplashScreenJavaFXApp> consumer;
    private static int i = 0;

    @Override
    public void start(Stage primaryStage) throws IOException {
        instance = this;
        this.mainStage = primaryStage;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root, 600, 500);
        scene.getStylesheets().add(String.valueOf(getClass().getResource("/stylesheet.css")));

        controller = loader.getController();

        primaryStage.setTitle("Splash Screen");
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(400);

        primaryStage.show();
        consumer.accept(instance);

        AtomicInteger count = new AtomicInteger();
        ScheduledService<String> service = new ScheduledService<String>() {
            @Override
            protected Task<String> createTask() {
                Task<String> task = new Task<String>() {
                    @Override
                    public String call() {
                        return "Count: "+count.incrementAndGet();
                    }
                };
                return task ;
            }
        };
        service.setOnSucceeded(event -> getController().LoadingText.setText(service.getValue()));
        service.setDelay(Duration.seconds(1));
        service.setPeriod(Duration.seconds(1));
        service.start();
    }

    public static SplashScreenJavaFXApp getInstance() {
        return instance;
    }

    public SplashScreenController getController() {
        return controller;
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public static void main(Consumer<SplashScreenJavaFXApp> consumer) {
        SplashScreenJavaFXApp.consumer = consumer;
        launch();
    }

    public static void setLoadingText(String className) {
        instance.controller.LoadingText.setText("Loading Plugin: " + className);
    }
}
